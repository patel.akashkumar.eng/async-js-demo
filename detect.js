const async = require('async');
//Return the first value that passes an async truth test. if first throw error then return undefined
async.detect(
    [2000, 1000, 4000, 6000, 2000, 5000, 8000, 9000, 7000], 
    function(task, cb){
      console.log('Process started', task);
      setTimeout(() => {
        if(task == 2000){
          return cb(new Error("Value is not true!"));
        }
        cb(undefined, task)
      }, task)
    },
    function(err, data) {
      console.log(err);
      console.log(data);
});