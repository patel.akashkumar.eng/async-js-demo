const async = require('async');
//Reduce value in to the single value
//if any element fail the test then return the error and pending elements are not added in result
async.reduce(
    [2000, 1000, 4000, 6000, 3000, 5000, 8000, 9000, 7000],
    0,
    function(memo, item, cb){
        console.log('Process started', memo, item);
        // if(item == 4000){
        //     return cb(new Error("Value is not true!"));
        // }
        cb(null, memo + item)
    },
    function(err, data) {
      console.log('Error', err);
      console.log('Data', data);
});