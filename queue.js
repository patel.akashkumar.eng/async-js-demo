const async = require('async');
// create a queue object with concurrency 2
var q = async.queue(function(task, callback) {
  console.log('hello ' + task);
  setTimeout(()=>{ 
    if(task == 5){
      return callback(new Error("Value is not true!"));
    }
    callback(undefined, task);
  }, 2000);
  // callback();
}, 2);

// assign a callback
q.drain(function() {
  console.log('all items have been processed');
});

// assign an error callback
q.error(function(err, task) {
  console.error('task experienced an error');
});

for(let i = 0; i < 10; i++){
  console.log(`printing ${i}`);
  q.push(i, function(err, data) {
    if(err){
      console.log(err);
      return;
    }
    console.log('finished processing item', data);
  });
}