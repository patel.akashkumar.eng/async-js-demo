const async = require('async');
//Produce new collection of values of mapping
//if any element fail the test then return the error and pending elements are not added in result
async.map(
    [2000, 1000, 4000, 6000, 2000, 5000, 8000, 9000, 7000], 
    function(task, cb){
      console.log('Process started', task);
      setTimeout(() => {
        // if(task == 6000){
        //   return cb(new Error("Value is not true!"));
        // }
        cb(null, task * 2)
      }, task)
    },
    function(err, data) {
      console.log('Error', err);
      console.log('Data', data);
      console.log('Data', data.length);
});