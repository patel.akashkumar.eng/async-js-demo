const async = require('async');
//Return true if every element of collection satisfies the async test
//if any element fail the test then return the error
async.every(
    [2000, 1000, 4000, 6000, 2000, 5000, 8000, 9000, 7000], 
    function(task, cb){
      console.log('Process started', task);
      setTimeout(() => {
        // if(task == 6000){
        //   return cb(new Error("Value is not true!"));
        // }
        cb(undefined, true)
      }, task)
    },
    function(err, data) {
      console.log(err);
      console.log(data);
});