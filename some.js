const async = require('async');
//Return true if atleas one of the values which pass an async truth test
async.some(
    [2000, 1000, 4000, 6000, 2000, 5000, 8000, 9000, 7000],
    function(task, cb){
      console.log('Process started', task);
      setTimeout(() => {
        // if(task == 6000){
        //   return cb(new Error("Value is not true!"));
        // }
        if(task == 4000 || task == 2000){
            return cb(null, true)
        }
        cb(null, false)
      }, task)
    },
    function(err, data) {
      console.log('Error', err);
      console.log('Data', data);
});