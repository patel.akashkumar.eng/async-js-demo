const async = require('async');
//Produce the object with key and value.
//if any element fail the test then return the error and pending elements are not added in result
async.mapValues(
    {
        f1: 2,
        f2: 4,
        f3: 6,
        f4: 8,
        f5: 10
    }, 
    function(task, key, cb){
      console.log('Process started', task, key);
      setTimeout(() => {
        if(task == 8){
          return cb(new Error("Value is not true!"));
        }
        cb(null, task * 2)
      }, task * 1000)
    },
    function(err, data) {
      console.log('Error', err);
      console.log('Data', data);
});