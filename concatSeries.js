const async = require('async');

async.concatSeries(
    [1000, 2000, 3000, 6000, 3000, 4000, 2000, 9000, 3000], 
    function(task, cb){
      console.log('Process started', task);
      setTimeout(() => {
        if(task == 6000){
          return cb(new Error("Value is not true!"));
        }
        cb(undefined, task)
      }, 5000)
    },
    function(err, data) {
      console.log(err);
      console.log(data);
});