const async = require('async');
//make the object and group the data
//if any element fail the test then return the error and pendin elements are not added in result
async.groupBy(
    [2000, 1000, 4000, 6000, 2000, 5000, 8000, 9000, 7000], 
    function(task, cb){
      console.log('Process started', task);
      setTimeout(() => {
        // if(task == 1000){
        //   return cb(new Error("Value is not true!"));
        // }
        if(task == 4000 || task == 2000 || task == 1000){
            return cb(null, 'UNDER_4')
        }
        cb(null, 'UNDER_9')
      }, task)
    },
    function(err, data) {
      console.log('Error', err);
      console.log('Data', data);
});